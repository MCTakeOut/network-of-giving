import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo(url:string): Promise<unknown> {
    return browser.get(url) as Promise<unknown>;
  }

  getTitleText(): Promise<string> {
    return element(by.id('title')).getText() as Promise<string>;
  }
}
