import { AppPage } from './app.po';
import { browser, logging, element, by } from 'protractor';

describe('login e2e', () => {
  let page: AppPage;
  var pass: string = 'test123';
  var username: string = 'test';

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo('http://localhost:4200/');
    expect(page.getTitleText()).toEqual('Network of Giving');
  });

  it('should be able to click login', () => {
    var logoutEl = element(by.id('logout'));
    logoutEl.click();
    browser.switchTo().alert().accept();
    var loginEl = element(by.id('login'));
    loginEl.click();
  });

  it('should be able to fill out username and password and click login', () => {
    var usernameInput = element(by.id('username'));
    usernameInput.sendKeys(username);
    var passInput = element(by.id('pass'));
    passInput.sendKeys(pass);
    var loginEl = element(by.id('loginbtn'));
    loginEl.click();
  });

  it('should display welcome message', () => {
    var greetingsEl = element(by.id('hello-btn'));
    expect(greetingsEl.getText()).toEqual('Hello, test !');
    // greetingsEl.click();
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(
      jasmine.objectContaining({
        level: logging.Level.SEVERE,
      } as logging.Entry)
    );
  });
});
