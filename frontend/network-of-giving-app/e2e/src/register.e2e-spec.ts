import { AppPage } from './app.po';
import { browser, logging, element, by } from 'protractor';

describe('Register e2e', () => {
  let page: AppPage;
  var pass: string = '123456';
  var username: string = '123456';

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo('http://localhost:4200/');
    expect(page.getTitleText()).toEqual('Network of Giving');
  });

  it('should be able to click login', () => {
    var loginEl = element(by.id('login'));
    loginEl.click();
  });

  it('should be able to click register', () => {
    let registerEl = element(by.id('register'));
    registerEl.click();
  });

  it('should be able to register', () => {
    var nameInput = element(by.id('name'));
    nameInput.sendKeys('Ivan Ivanov');
    var usernameInput = element(by.id('username'));
    usernameInput.sendKeys(username);
    var passInput = element(by.id('password'));
    passInput.sendKeys(pass);
    var locationInput = element(by.id('location'));
    locationInput.sendKeys('Sofia');
    var ageInput = element(by.id('age'));
    ageInput.sendKeys('22');
    var genderInput = element(by.id('male'));
    genderInput.click();
    var registerEl = element(by.id('register'));
    registerEl.click();
  });

  it('should be able to fill out username and password and click login', () => {
    page.navigateTo('http://localhost:4200/');
    var loginEl = element(by.id('login'));
    loginEl.click();
    var usernameInput = element(by.id('username'));
    usernameInput.sendKeys(username);
    var passInput = element(by.id('pass'));
    passInput.sendKeys(pass);
    var loginEl = element(by.id('loginbtn'));
    loginEl.click();
  });

  it('should display welcome message', () => {
    var greetingsEl = element(by.id('hello-btn'));
    expect(greetingsEl.getText()).toEqual('Hello, Ivan Ivanov !');
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(
      jasmine.objectContaining({
        level: logging.Level.SEVERE,
      } as logging.Entry)
    );
  });
});
