import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { CharityComponent } from './components/charity/charity.component';
import { CreateCharityComponent } from './components/create-charity/create-charity.component';
import { EditCharityComponent } from './components/edit-charity/edit-charity.component';
import { UserViewComponent } from './components/user-view/user-view.component';

const accountModule = () =>
  import('./components/account/account.module').then((x) => x.AccountModule);
// const usersModule = () => import('./users/users.module').then(x => x.UsersModule);

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'account', loadChildren: accountModule },
  { path: 'view/:id', component: CharityComponent },
  { path: 'create-charity', component: CreateCharityComponent },
  { path: 'edit-charity/:id', component: EditCharityComponent },
  { path: 'user/:id', component: UserViewComponent },

  // otherwise redirect to home
  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
