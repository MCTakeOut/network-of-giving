import { Component } from '@angular/core';

import { AccountService } from './services/account.service';
import { User } from './models/user';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app',
  templateUrl: 'app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  user: User;
  loggedIn: boolean;
  title = 'Network of Giving';
  loginSubscription: Subscription;

  constructor(private accountService: AccountService) {
    this.accountService.user.subscribe((x) => (this.user = x));
    this.loggedIn = false;
  }

  logout() {
    if (confirm('Logout?')) {
      this.accountService.logout();
    }
    this.loggedIn = false;
  }

  
}
