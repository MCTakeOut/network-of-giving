import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CharityComponent } from './components/charity/charity.component';
import { NameFilterPipe } from './pipes/name-filter.pipe';
import { CreateCharityComponent } from './components/create-charity/create-charity.component';
import { EditCharityComponent } from './components/edit-charity/edit-charity.component';
import { UserViewComponent } from './components/user-view/user-view.component';
import { ShareModule } from "ngx-sharebuttons";
import { ShareIconsModule } from "ngx-sharebuttons/icons";



@NgModule({
  declarations: [AppComponent, HomeComponent, CharityComponent, NameFilterPipe, CreateCharityComponent, EditCharityComponent, UserViewComponent],
  imports: [    
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ShareModule,
    ShareIconsModule
  ],
  providers: [
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
