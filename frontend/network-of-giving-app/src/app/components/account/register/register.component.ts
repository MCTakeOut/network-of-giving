import { Component, OnInit, NgModule } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AccountService } from '../../../services/account.service';
import { Subscription } from 'rxjs';

@Component({
  templateUrl: 'register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {
  form: FormGroup;
  loading = false;
  submitted = false;
  hasError: boolean;
  registerSubscription: Subscription;
  namePattern = "/^[a-z ,.'-]+$/i";
  usernamePattern = '^[a-z0-9_-]{4,15}$';

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private accountService: AccountService
  ) {
    // redirect to home if already logged in
    if (this.accountService.userValue) {
      this.router.navigate(['/']);
    }
  }

  // /^[a-z ,.'-]+$/i

  ngOnInit() {
    this.form = this.formBuilder.group({
      fullName: [
        '',
        [Validators.required],
      ],
      username: [
        '',
        [Validators.required],
      ],
      pass: ['', [Validators.required, Validators.minLength(6)]],
      location: ['', Validators.required],
      age: ['', Validators.required],
      sex: ['', Validators.required],
    });
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.form.controls;
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.form.invalid) {
      return;
    }

    this.loading = true;
    this.registerSubscription = this.accountService
      .register(this.form.value)
      .pipe(first())
      .subscribe(
        (data) => {
          this.hasError = false;
          this.router.navigate(['../login'], { relativeTo: this.route });
        },
        (error) => {
          this.hasError = true;
          this.loading = false;
        }
      );
  }
}
