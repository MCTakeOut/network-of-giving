import { Component, OnInit } from '@angular/core';
import { Charity, CharityType } from 'src/app/models';
import { CharityService } from 'src/app/services/chartiy.service';
import { ActivatedRoute, Router } from '@angular/router';
import { throwError, Subscription } from 'rxjs';
import { AppComponent } from 'src/app/app.component';
import { EventLogService } from 'src/app/services/event.log.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AccountService } from 'src/app/services/account.service';
import { first } from 'rxjs/operators';
import { faFacebookSquare } from '@fortawesome/free-brands-svg-icons/faFacebookSquare';
import { faTwitterSquare } from '@fortawesome/free-brands-svg-icons/faTwitterSquare';
import { faPinterest } from '@fortawesome/free-brands-svg-icons/faPinterest';


@Component({
  selector: 'app-charity',
  templateUrl: './charity.component.html',
  styleUrls: ['./charity.component.css'],
})
export class CharityComponent implements OnInit {
  id: number;
  amount: number;
  charity: Charity;
  withMoney: boolean;
  withVolunteers: boolean;
  isLoggedIn: boolean;
  moneyProgress: number;
  volunteersProgress: number;
  isVolunteering: boolean;
  donateForm: FormGroup;
  suggestedAmount: number;
  donateSubscription: Subscription;
  getSuggestedSubscription: Subscription;
  volunteerSubscription: Subscription;
  checkSubscription: Subscription;
  isOwner: boolean;
  fbIcon = faFacebookSquare;
  pinIcon = faPinterest;
  tweetIcon = faTwitterSquare;
  href: string;

  constructor(
    private charityService: CharityService,
    private eventLogService: EventLogService,
    private route: ActivatedRoute,
    private router: Router,
    private app: AppComponent,
    private formBuilder: FormBuilder,
    private accountService: AccountService,
  ) {}

  ngOnInit(): void {
    this.href = this.router.url;
    this.donateForm = this.formBuilder.group({
      amount: ['', Validators.pattern('^[0-9]*$')],
    });

    this.withMoney = false;
    this.withVolunteers = false;

    this.isLoggedIn = this.app.user != null;
    this.id = this.route.snapshot.params['id'];
    this.charityService
      .getCharityById(this.id)
      .pipe(first())
      .subscribe((charity) => {
        this.charity = charity;
        if (!this.charity) {
          throwError(404);
        }
        if (
          this.isLoggedIn &&
          (this.charity.charityType === CharityType.VOLUNTEER_CHARITY ||
            this.charity.charityType ===
              CharityType.MONEY_AND_VOLUNTEERS_CHARITY)
        ) {
          this.checkIfUserIsAlreadyVolunteered();
        }

        if (
          this.isLoggedIn &&
          (this.charity.charityType === CharityType.MONEY_CHARITY ||
            this.charity.charityType ===
              CharityType.MONEY_AND_VOLUNTEERS_CHARITY)
        ) {
          this.getSuggestedAmount();
        }

        if (this.isLoggedIn) {
          this.checkIfLoggedUserIsOwner();
        }

        if (this.charity.charityType === CharityType.MONEY_CHARITY) {
          this.moneyProgress =
            (this.charity.moneyRaised / this.charity.moneyRequired) * 100;
          this.withMoney = true;
        } else if (this.charity.charityType === CharityType.VOLUNTEER_CHARITY) {
          this.volunteersProgress =
            (this.charity.volunteersGathered /
              this.charity.volunteersRequired) *
            100;
          this.withVolunteers = true;
        } else {
          this.moneyProgress =
            (this.charity.moneyRaised / this.charity.moneyRequired) * 100;
          this.volunteersProgress =
            (this.charity.volunteersGathered /
              this.charity.volunteersRequired) *
            100;
          this.withVolunteers = true;
          this.withMoney = true;
        }
      });
  }

  get f() {
    return this.donateForm.controls;
  }

  deleteCharity() {
    if (confirm('Are you shure you want to delete this charity?')) {    
    this.charityService
      .deleteCharity(this.app.user.id, this.id)
      .pipe(first())
      .subscribe(
        (charity) => {
          this.router.navigate(['/']);
        },
        (error) => {
          console.log('error');
        }
      );
    }
  }

  donate(amount: number) {
    this.donateSubscription = this.charityService
      .donateToCharity(this.app.user.id, this.charity.id, amount)
      .pipe(first())
      .subscribe(
        (charity) => {
          location.reload();
        },
        (error) => {
          console.log('error');
        }
      );
  }

  getSuggestedAmount() {
    this.getSuggestedSubscription = this.accountService
      .getUserById(this.app.user.id)
      .pipe(first())
      .subscribe(
        (user) => {
          this.suggestedAmount = +(
            user.totalAmountDonated / user.totalDonationsMade
          ).toFixed(2);
        },
        (error) => {
          console.log('error');
        }
      );
  }

  checkIfUserIsAlreadyVolunteered() {
    this.checkSubscription = this.eventLogService
      .isUserParticipating(this.app.user.id, this.charity.id)
      .pipe(first())
      .subscribe(
        (data) => {
          this.isVolunteering = data;
        },
        (error) => {
          this.isVolunteering = false;
        }
      );
  }

  checkIfLoggedUserIsOwner() {
    this.charityService
      .getCreator(this.charity.id)
      .pipe(first())
      .subscribe(
        (data) => {
          if (this.app.user.id === data.id) {
            this.isOwner = true;
          } else {
            this.isOwner = false;
          }
        },
        (error) => {
          this.isOwner = false;
        }
      );
  }

  volunteer() {
    this.volunteerSubscription = this.charityService
      .volunteerInCharity(this.app.user.id, this.charity.id)
      .pipe(first())
      .subscribe(
        (charity) => {
          location.reload();
        },
        (error) => {
          console.log('error');
        }
      );
  }
}
