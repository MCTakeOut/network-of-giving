import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CharityService } from 'src/app/services/chartiy.service';
import { AppComponent } from 'src/app/app.component';
import { first } from 'rxjs/operators';
import { CharityType } from 'src/app/models';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-create-charity',
  templateUrl: './create-charity.component.html',
  styleUrls: ['./create-charity.component.css'],
})
export class CreateCharityComponent implements OnInit {
  form: FormGroup;
  loading = false;
  submitted = false;
  thumbnailHasError: boolean = false;
  hasError: boolean = false;
  @ViewChild('labelImport')
  labelImport: ElementRef;
  createCharitySubscription: Subscription;
  validateSubscription: Subscription;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private charityService: CharityService,
    private app: AppComponent
  ) {
    if (!this.app.user) {
      this.router.navigate(['/']);
    }
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      name: ['', Validators.required],
      thumbnailName: ['', [Validators.required]],
      description: ['', [Validators.required]],
      charityType: ['', Validators.required],
      moneyRequired: ['', ],
      volunteersRequired: ['', ],
    });
    this.setUserCategoryValidators();
  }

  get f() {
    return this.form.controls;
  }

  onSubmit() {
    this.submitted = true;
    this.thumbnailHasError = false;

    this.form.controls['thumbnailName'].setValue(
      this.labelImport.nativeElement.innerText
    );
    // stop here if form is invalid
    if (this.form.invalid || this.form.controls['thumbnailName'].value === '') {
      if (this.form.controls['thumbnailName'].value === '') {
        this.thumbnailHasError = true;
      }
      return;
    }

    this.loading = true;
    this.createCharitySubscription = this.charityService
      .createCharity(this.app.user.id, this.form.value)
      .pipe(first())
      .subscribe(
        (data) => {
          this.hasError = false;
          this.router.navigate(['/']);
        },
        (error) => {
          this.hasError = true;
        }
      );
  }

  onFileChange(files: FileList) {
    this.labelImport.nativeElement.innerText = Array.from(files)
      .map((f) => f.name.replace(/^C:\\fakepath\\/, '').replace(/^.*\\/, ''))
      .join(', ');
  }

  setUserCategoryValidators() {
    const moneyControl = this.form.get('moneyRequired');
    const volunteerControl = this.form.get('volunteersRequired');

   this.validateSubscription = this.form.get('charityType').valueChanges
      .subscribe(charityType => {

        if (charityType === CharityType.MONEY_CHARITY) {
          moneyControl.setValidators([Validators.required]);
          volunteerControl.setValidators(null);
        }

        if (charityType === CharityType.VOLUNTEER_CHARITY) {
          moneyControl.setValidators(null);
          volunteerControl.setValidators([Validators.required]);
        }

        if (charityType === CharityType.MONEY_AND_VOLUNTEERS_CHARITY) {
          moneyControl.setValidators([Validators.required]);
          volunteerControl.setValidators([Validators.required]);
        }

        moneyControl.updateValueAndValidity();
        volunteerControl.updateValueAndValidity();
      });
  }

  ngOnDestroy(){
    this.validateSubscription.unsubscribe();
  }


}
