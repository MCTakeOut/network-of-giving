import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CharityService } from 'src/app/services/chartiy.service';
import { AppComponent } from 'src/app/app.component';
import { Subscription } from 'rxjs';
import { CharityType, Charity } from 'src/app/models';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-edit-charity',
  templateUrl: './edit-charity.component.html',
  styleUrls: ['./edit-charity.component.css'],
})
export class EditCharityComponent implements OnInit {
  id: number;
  form: FormGroup;
  charity: Charity;
  loading = false;
  submitted = false;
  thumbnailHasError: boolean = false;
  hasError: boolean = false;
  @ViewChild('labelImport')
  labelImport: ElementRef;
  validateSubscription: Subscription;

  constructor(
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private router: Router,
    private charityService: CharityService,
    private app: AppComponent
  ) {
    this.id = this.route.snapshot.params['id'];
    if (!this.app.user) {
      this.router.navigate(['/']);
    }
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      name: ['', Validators.required],
      thumbnailName: ['', [Validators.required]],
      description: ['', [Validators.required]],
      charityType: ['', Validators.required],
      moneyRequired: [''],
      volunteersRequired: [''],
    });
    this.getCharityData();
    this.setUserCategoryValidators();
  }

  get f() {
    return this.form.controls;
  }

  onSubmit() {
    this.submitted = true;
    this.thumbnailHasError = false;
    this.form.controls['thumbnailName'].setValue(
      this.labelImport.nativeElement.innerText
    );

    // stop here if form is invalid
    if (this.form.invalid || this.form.controls['thumbnailName'].value === '') {
      if (this.form.controls['thumbnailName'].value === '') {
        this.thumbnailHasError = true;
      }
      return;
    }

    this.loading = true;
    this.charityService
      .updateCharity(this.app.user.id, this.id, this.form.value)
      .pipe(first())
      .subscribe(
        (data) => {
          this.hasError = false;
          this.router.navigate(['../../view/'+this.id], { relativeTo: this.route });
        },
        (error) => {
          this.hasError = true;
          this.loading = false;
        }
      );
  }

  getCharityData() {
    this.charityService
      .getCharityById(this.id)
      .pipe(first())
      .subscribe(
        (data) => {
          this.charity = data;
          this.form.controls.charityType.setValue(this.charity.charityType);
        },
        (error) => {
          console.log('get charity error');
        }
      );
  }

  setUserCategoryValidators() {
    const moneyControl = this.form.get('moneyRequired');
    const volunteerControl = this.form.get('volunteersRequired');

    this.validateSubscription = this.form
      .get('charityType')
      .valueChanges.subscribe((charityType) => {
        if (charityType === CharityType.MONEY_CHARITY) {
          moneyControl.setValidators([Validators.required]);
          volunteerControl.setValidators(null);
        }

        if (charityType === CharityType.VOLUNTEER_CHARITY) {
          moneyControl.setValidators(null);
          volunteerControl.setValidators([Validators.required]);
        }

        if (charityType === CharityType.MONEY_AND_VOLUNTEERS_CHARITY) {
          moneyControl.setValidators([Validators.required]);
          volunteerControl.setValidators([Validators.required]);
        }

        moneyControl.updateValueAndValidity();
        volunteerControl.updateValueAndValidity();
      });
  }

  onFileChange(files: FileList) {
    this.labelImport.nativeElement.innerText = Array.from(files)
      .map((f) => f.name.replace(/^C:\\fakepath\\/, '').replace(/^.*\\/, ''))
      .join(', ');
  }
}
