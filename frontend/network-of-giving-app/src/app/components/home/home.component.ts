import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { FormsModule } from '@angular/forms';

import { User } from '../../models';
import { NameFilterPipe } from "../../pipes/name-filter.pipe";
import { AccountService } from '../../services/account.service';
import { environment } from '../../../environments/environment';
import { CharityService } from '../../services/chartiy.service';
import { Subscription } from 'rxjs';


@Component({
  templateUrl: 'home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  user: User;
  searchStr: string = "";
  charities = null;
  title = `${environment.title}`;
  getAllCharitiesSubscription: Subscription;

  constructor(
    private accountService: AccountService,
    private charityService: CharityService,
  ) {
    this.user = this.accountService.userValue;
  }

  ngOnInit() {
    this.getAllCharitiesSubscription = this.charityService
      .getAll()
      .pipe(first())
      .subscribe((charities) => (this.charities = charities));
  }


  // public modelChange(str: string): void {
  //     this.searchStr = str;
  // }
}
