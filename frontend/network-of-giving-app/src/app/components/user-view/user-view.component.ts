import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import { User, Charity } from 'src/app/models';
import { CharityService } from 'src/app/services/chartiy.service';
import { first } from 'rxjs/operators';
import { AccountService } from 'src/app/services/account.service';

@Component({
  selector: 'app-user-view',
  templateUrl: './user-view.component.html',
  styleUrls: ['./user-view.component.css'],
})
export class UserViewComponent implements OnInit {
  id: number;
  user: User;
  events: Event[] = [];
  charities: Charity[] = [];
  href: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private app: AppComponent,
    private charityService: CharityService,
    private accountService: AccountService
  ) {
    if (!this.app.user) {
      this.router.navigate(['/']);
    }
    this.user = app.user;
    this.href = this.router.url;
  }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.charityService
      .getCharitiesByUser(this.user.id)
      .pipe(first())
      .subscribe(
        (charities) => {
          if (charities != null) {
            this.charities = charities;
          }
        },
        (error) => {}
      );

    this.accountService
      .getEvents(this.user.id)
      .pipe(first())
      .subscribe(
        (events) => {
          if (events != null) {
            this.events = events;
          }
        },
        (error) => {}
      );
  }
}
