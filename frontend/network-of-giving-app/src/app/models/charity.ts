export class Charity {
  id: number;
  name: string;
  thumbnailName: string;
  description: string;
  charityType: CharityType;
  moneyRequired: number;
  moneyRaised: number;
  volunteersRequired: number;
  volunteersGathered: number;
}

export enum CharityType {
  MONEY_CHARITY = 'MONEY_CHARITY',
  VOLUNTEER_CHARITY = 'VOLUNTEER_CHARITY',
  MONEY_AND_VOLUNTEERS_CHARITY = 'MONEY_AND_VOLUNTEERS_CHARITY'
}
