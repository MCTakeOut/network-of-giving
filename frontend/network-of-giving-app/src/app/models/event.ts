import { User } from './user';
import { Charity } from './charity';

export class Event {
  user: User;
  eventType: EventType;
  deletedCharity: string;
  charity: Charity;
  eventTime: Date;
  moneyDonated: number;
  volunteered: Boolean;
}

export enum EventType {
  CREATE_CHARITY = 'CREATE_CHARITY',
  CREATE_USER = 'CREATE_USER',
  DONATE_TO_CHARITY = 'DONATE_TO_CHARITY',
  VOLUNTEERED_IN_CHARITY = 'VOLUNTEERED_IN_CHARITY',
  UPDATE_USER = 'UPDATE_USER',
  UPDATE_CHARITY = 'UPDATE_CHARITY',
  DELETE_CHARITY = 'DELETE_CHARITY',
  REMOVED_FROM_CHARITY_UPDATE = 'REMOVED_FROM_CHARITY_UPDATE' ,
  REMOVED_FROM_CHARITY_DELETE = 'REMOVED_FROM_CHARITY_DELETE',
  MONEY_REFUND_UPDATE = 'MONEY_REFUND_UPDATE',
  MONEY_REFUND_DELETE = 'MONEY_REFUND_DELETE',
  VOLUNTEERS_MET = 'VOLUNTEERS_MET',
  MONEY_MET = 'MONEY_MET'
}
