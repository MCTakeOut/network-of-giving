export class User {
  id: number;
  username: string;
  pass: string;
  fullName: string;
  location: string;
  sex: Gender;
  age: number;
  totalAmountDonated: number;
  totalDonationsMade: number;
  totalVolunteerings: number;
}

export enum Gender {
  MALE,
  FEMALE,
  OTHER,
}
