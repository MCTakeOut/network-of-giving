import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'nameFilter'
})
export class NameFilterPipe implements PipeTransform {

  transform(value: any[], term: string): any[] {
    return value.filter((x:any) => x.name.toLowerCase().includes(term));
  }

}
