import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { Charity, User } from '../models';

@Injectable({ providedIn: 'root' })
export class CharityService {
  constructor(private router: Router, private http: HttpClient) {}

  getAll() {
    return this.http.get<Charity[]>(`${environment.apiUrl}/charity`);
  }

  getCharityById(id: number) {
    return this.http.get<Charity>(`${environment.apiUrl}/charity/${id}`);
  }

  donateToCharity(userId: number, charityId: number, amount: number) {
    return this.http.put<Charity>(
      `${environment.apiUrl}/user/${userId}/charity/${charityId}/donate?donation=${amount}`,
      {}
    );
  }

  volunteerInCharity(userId: number, charityId: number) {
    var isVolunteering: boolean = true;
    return this.http.put<Charity>(
      `${environment.apiUrl}/user/${userId}/charity/${charityId}/volunteer?isVolunteering=${isVolunteering}`,
      {}
    );
  }

  deleteCharity(userId: number, charityId: number) {
    return this.http.delete(`${environment.apiUrl}/user/${userId}/charity/${charityId}`)
  }

  createCharity(userId: number, charity: Charity){
      return this.http.post(`${environment.apiUrl}/user/${userId}/charity`, charity);
  }

  getCreator(charityId:number) {
    return this.http.get<User>(`${environment.apiUrl}/charity/get-creator/${charityId}`);
  }

  updateCharity(userId: number, charityId: number, charity: Charity){
    return this.http.put<Charity>(`${environment.apiUrl}/user/${userId}/charity/${charityId}`,charity);
  }

  getCharitiesByUser(userId: number){
    return this.http.get<Charity[]>(`${environment.apiUrl}/user/${userId}/created-charities`)
  }
}
