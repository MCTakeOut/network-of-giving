import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { Charity } from '../models';

@Injectable({ providedIn: 'root' })
export class EventLogService {
  constructor(private router: Router, private http: HttpClient) {}

  isUserParticipating(userId: number, charityId: number) {
      return this.http.get<boolean>(`${environment.apiUrl}/user/${userId}/chaity/${charityId}/isVolunteering`)
  }
}
