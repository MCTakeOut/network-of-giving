package com.vmware.finaltask.networkofgiving.controller;


import com.vmware.finaltask.networkofgiving.manager.CharityManager;
import com.vmware.finaltask.networkofgiving.model.Charity;
import com.vmware.finaltask.networkofgiving.model.User;
import com.vmware.finaltask.networkofgiving.operations.ChraityOperations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api")
public class CharityController implements ChraityOperations {

    @Autowired
    private CharityManager charityManager;

    @Override
    public ResponseEntity<List<Charity>> getAllCharities() {
       return charityManager.getAllCharities();
    }

    @Override
    public ResponseEntity<Charity> getCharityById(@PathVariable long id) {
        return charityManager.getCharityById(id);
    }

    @Override
    public ResponseEntity<Charity> getCharityByName(@RequestParam String name) {
        return charityManager.getCharityByName(name);
    }

    @Override
    public ResponseEntity<Charity> createCharity(@PathVariable long id, @RequestBody Charity charity) {
        return charityManager.createCharity(id, charity);
    }

    @Override
    public ResponseEntity<Charity> updateCharity(@PathVariable long userId, @PathVariable long charityId,
                                                 @RequestBody Charity charity) {
        return charityManager.updateCharity(userId, charityId, charity);
    }

    @Override
    public ResponseEntity<HttpStatus> deleteCharity(@PathVariable long userId, @PathVariable long charityId) {
        return charityManager.deleteCharity(userId, charityId);
    }

    @Override
    public ResponseEntity<User> getCharityCreator(@PathVariable long id) {
        return charityManager.getCharityCreator(id);
    }

    @Override
    public ResponseEntity<List<Charity>> searchForCharity(@RequestParam String name) {
        return charityManager.searchForCharity(name);
    }

    @Override
    public ResponseEntity<List<User>> getDonators(@PathVariable long id) {
        return charityManager.getDonators(id);
    }

    @Override
    public ResponseEntity<List<User>> getVolunteers(@PathVariable long id) {
        return charityManager.getVolunteers(id);
    }

    @Override
    public ResponseEntity<Double> getMoneyNeeded(@PathVariable long id) {
        return charityManager.getMoneyNeeded(id);
    }

    @Override
    public ResponseEntity<Integer> getParticipantsNeeded(@PathVariable long id) {
        return charityManager.getParticipantsNeeded(id);
    }
}
