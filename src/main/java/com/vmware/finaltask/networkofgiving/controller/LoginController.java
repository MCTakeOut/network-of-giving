package com.vmware.finaltask.networkofgiving.controller;

import com.vmware.finaltask.networkofgiving.manager.LoginManager;
import com.vmware.finaltask.networkofgiving.model.User;
import com.vmware.finaltask.networkofgiving.model.UserLoginDummy;
import com.vmware.finaltask.networkofgiving.operations.LoginOperations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api")
public class LoginController implements LoginOperations {

    @Autowired
    private LoginManager loginManager;

    @Override
    public ResponseEntity<User> login(@RequestBody UserLoginDummy dummy) {
        return loginManager.login(dummy);
    }
}
