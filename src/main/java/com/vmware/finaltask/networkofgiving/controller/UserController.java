package com.vmware.finaltask.networkofgiving.controller;


import com.vmware.finaltask.networkofgiving.manager.UserManager;
import com.vmware.finaltask.networkofgiving.model.Charity;
import com.vmware.finaltask.networkofgiving.model.LogEntry;
import com.vmware.finaltask.networkofgiving.model.User;
import com.vmware.finaltask.networkofgiving.operations.UserOperations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api")
public class UserController implements UserOperations {


    @Autowired
    private UserManager userManager;

    @Override
    public ResponseEntity<List<User>> getAllUsers() {
        return userManager.getAllUsers();
    }

    @Override
    public ResponseEntity<User> getUserById(@PathVariable("id") long id) {
        return userManager.getUserById(id);
    }

    @Override
    public ResponseEntity<User> createUser(@RequestBody User user) {
        return userManager.createUser(user);
    }

    @Override
    public ResponseEntity<User> updateUser(@PathVariable("id") long id, @RequestBody User user) {
        return userManager.updateUser(id, user);
    }

    @Override
    public ResponseEntity<HttpStatus> deleteUser(@PathVariable long id) {
        return userManager.deleteUser(id);
    }

    @Override
    public ResponseEntity<List<Charity>> getCreatedCharities(@PathVariable long id) {
        return userManager.getCreatedCharities(id);
    }

    @Override
    public ResponseEntity<User> getUserByUsername(@RequestParam String username) {
        return userManager.getUserByUsername(username);
    }

    @Override
    public ResponseEntity<Charity> donateMoney(@PathVariable long userId, @PathVariable long charityId,
                                               @RequestParam Double donation) {
        return userManager.donateMoney(userId, charityId, donation);
    }

    @Override
    public ResponseEntity<Boolean> isVolunteering(@PathVariable long userId, @PathVariable long charityId) {
        return userManager.isVolunteering(userId, charityId);
    }

    @Override
    public ResponseEntity<Charity> volunteerInCharity(@PathVariable long userId, @PathVariable long charityId,
                                                      @PathVariable Boolean isVolunteering) {
        return userManager.volunteerInCharity(userId, charityId, isVolunteering);
    }

    @Override
    public ResponseEntity<List<LogEntry>> getEventsByUser(@PathVariable long userId) {
        return userManager.getEventsByUser(userId);
    }
}
