package com.vmware.finaltask.networkofgiving.manager;


import com.vmware.finaltask.networkofgiving.model.Charity;
import com.vmware.finaltask.networkofgiving.model.LogEntry;
import com.vmware.finaltask.networkofgiving.model.User;
import com.vmware.finaltask.networkofgiving.operations.ChraityOperations;
import com.vmware.finaltask.networkofgiving.repository.CharityRepository;
import com.vmware.finaltask.networkofgiving.repository.LogEntryRepository;
import com.vmware.finaltask.networkofgiving.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class CharityManager implements ChraityOperations {

    @Autowired
    UserRepository userRepository;

    @Autowired
    CharityRepository charityRepository;

    @Autowired
    LogEntryRepository logEntryRepository;

    @Override
    public ResponseEntity<List<Charity>> getAllCharities() {
        try {
            List<Charity> charities = new ArrayList<>(charityRepository.findAll());
            if (charities.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(charities, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<Charity> getCharityById(long id) {
        Optional<Charity> charityData = charityRepository.findById(id);
        if (charityData.isPresent()) {
            return new ResponseEntity<>(charityData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<Charity> getCharityByName(String name) {
        Optional<Charity> charityData = charityRepository.findByName(name);
        if (charityData.isPresent()) {
            return new ResponseEntity<>(charityData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<Charity> createCharity(long id, Charity charity) {
        try {
            Optional<User> creatorUserData = userRepository.findById(id);
            if (creatorUserData.isPresent()) {
                Charity addedCharity = new Charity.CharityBuilder(charity.getName(), charity.getThumbnailName(),
                        charity.getDescription(), charity.getCharityType())
                        .withVolunteers(charity.getVolunteersRequired())
                        .withMoney(charity.getMoneyRequired())
                        .build();
                charityRepository.save(addedCharity);
                logEntryRepository.save(new LogEntry.LogEntryBuilder(LogEntry.EventType.CREATE_CHARITY, new Date())
                        .withUser(creatorUserData.get()).withCharity(addedCharity).build());
                return new ResponseEntity<>(addedCharity, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @Override
    public ResponseEntity<Charity> updateCharity(long userId, long charityId, Charity givenCharity) {
        Optional<User> userData = userRepository.findById(userId);
        Optional<Charity> charityData = charityRepository.findById(charityId);
        if (userData.isPresent() && charityData.isPresent() && isUserOwnerOfCharity(userId, charityId)) {
            Charity charityToUpdate = charityData.get();
            User charityCreator = userData.get();
            if (charityToUpdate.getCharityType() == givenCharity.getCharityType()) {
                charityToUpdate.setName(givenCharity.getName());
                charityToUpdate.setThumbnailName(givenCharity.getThumbnailName());
                charityToUpdate.setDescription(givenCharity.getDescription());
                if (charityToUpdate.isCharityWithMoney()) {
                    updateCharityMoney(charityToUpdate, givenCharity);
                }
                if (charityToUpdate.isCharityWithVolunteers()) {
                    updateCharityVolunteers(charityToUpdate, givenCharity);
                }
                charityRepository.save(charityToUpdate);
                logEntryRepository.save(new LogEntry.LogEntryBuilder(LogEntry.EventType.UPDATE_CHARITY, new Date())
                        .withUser(charityCreator)
                        .withCharity(charityToUpdate)
                        .build());
                return new ResponseEntity<>(charityToUpdate, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    private void updateCharityMoney(Charity charityToUpdate, Charity givenCharity) {
        if (charityToUpdate.getMoneyRaised() > givenCharity.getMoneyRequired()) {
            removeDonationLogs(charityToUpdate, givenCharity);
            charityToUpdate.setMoneyRequired(givenCharity.getMoneyRequired());
        } else {
            charityToUpdate.setMoneyRequired(givenCharity.getMoneyRequired());
        }

        if (charityToUpdate.getMoneyRaised().equals(givenCharity.getMoneyRequired()) && charityIsntMarkedWithMoneyMet(charityToUpdate)) {
            markCharityMoneyMet(charityToUpdate);
        }

        if (charityToUpdate.getMoneyRaised() < givenCharity.getMoneyRequired() && !charityIsntMarkedWithMoneyMet(charityToUpdate)) {
            markCharityMoneyNotMet(charityToUpdate);
        }
    }

    private void removeDonationLogs(Charity charityToUpdate, Charity givenCharity) {
        List<LogEntry> donations = logEntryRepository.findByCharityIdAndEventTypeOrderByEventTimeDesc(charityToUpdate.getId(),
                LogEntry.EventType.DONATE_TO_CHARITY);
        int i = 0;
        while (charityToUpdate.getMoneyRaised() > givenCharity.getMoneyRequired()) {
            logEntryRepository.deleteById(donations.get(i).getId());
            charityToUpdate.setMoneyRaised(charityToUpdate.getMoneyRaised() - donations.get(i).getMoneyDonated());
            Optional<User> donatorData = userRepository.findById(donations.get(i).getUser().getId());
            if (donatorData.isPresent()) {
                User donator = donatorData.get();
                donator.setTotalDonationsMade(donator.getTotalDonationsMade() - 1);
                donator.setTotalAmountDonated(donator.getTotalAmountDonated() - donations.get(i).getMoneyDonated());
                logEntryRepository.save(new LogEntry.LogEntryBuilder(LogEntry.EventType.MONEY_REFUND_UPDATE, new Date())
                        .withUser(donator)
                        .withMoneyDonated(donations.get(i).getMoneyDonated())
                        .withCharity(charityToUpdate)
                        .build());
            }
            i++;
        }
    }

    private boolean charityIsntMarkedWithMoneyMet(Charity charityToCheck) {
        Optional<LogEntry> log = logEntryRepository.findByCharityIdAndEventType(charityToCheck.getId(), LogEntry.EventType.MONEY_MET);
        return log.isEmpty();
    }

    private void markCharityMoneyMet(Charity charityToMark) {
        logEntryRepository.save(new LogEntry.LogEntryBuilder(LogEntry.EventType.MONEY_MET, new Date())
                .withCharity(charityToMark)
                .build());
    }

    private void markCharityMoneyNotMet(Charity charityToMark) {
        Optional<LogEntry> logData = logEntryRepository.findByCharityIdAndEventType(charityToMark.getId(), LogEntry.EventType.MONEY_MET);
        logData.ifPresent(logEntry -> logEntryRepository.deleteById(logEntry.getId()));
    }

    private void updateCharityVolunteers(Charity charityToUpdate, Charity givenCharity) {
        if (charityToUpdate.getVolunteersGathered() > givenCharity.getVolunteersRequired()) {
            int volunteersToRemove = charityToUpdate.getVolunteersGathered() - givenCharity.getVolunteersRequired();
            removeVolunteerLogs(charityToUpdate.getId(), volunteersToRemove);
            charityToUpdate.setVolunteersGathered(charityToUpdate.getVolunteersGathered() - volunteersToRemove);
        }
        charityToUpdate.setVolunteersRequired(givenCharity.getVolunteersRequired());
        if (charityToUpdate.getVolunteersGathered().equals(givenCharity.getVolunteersRequired()) && charityIsntMarkedWithVolunteersMet(charityToUpdate)) {
            markCharityVolunteersMet(charityToUpdate);
        }
        if (charityToUpdate.getVolunteersGathered() < givenCharity.getVolunteersRequired() && !charityIsntMarkedWithVolunteersMet(charityToUpdate)) {
            markCharityVolunteersNotMet(charityToUpdate);
        }
    }


    private void removeVolunteerLogs(long charityId, int volunteersToRemove) {
        List<LogEntry> volunteerings = logEntryRepository.findByCharityIdAndEventTypeOrderByEventTimeDesc(charityId,
                LogEntry.EventType.VOLUNTEERED_IN_CHARITY);
        for (int i = 0; i < volunteersToRemove; i++) {
            logEntryRepository.deleteById(volunteerings.get(i).getId());
            Optional<User> volunteerData = userRepository.findById(volunteerings.get(i).getUser().getId());
            if (volunteerData.isPresent()) {
                User volunteer = volunteerData.get();
                volunteer.setTotalVolunteerings(volunteer.getTotalVolunteerings() - 1);
                userRepository.save(volunteer);
                logEntryRepository.save(new LogEntry.LogEntryBuilder(LogEntry.EventType.REMOVED_FROM_CHARITY_UPDATE, new Date())
                        .withUser(volunteerings.get(i).getUser())
                        .withCharity(volunteerings.get(i).getCharity())
                        .build());
            }
        }
    }

    private boolean charityIsntMarkedWithVolunteersMet(Charity charityToCheck) {
        Optional<LogEntry> log = logEntryRepository.findByCharityIdAndEventType(charityToCheck.getId(), LogEntry.EventType.VOLUNTEERS_MET);
        return log.isEmpty();
    }

    private void markCharityVolunteersMet(Charity charityToMark) {
        logEntryRepository.save(new LogEntry.LogEntryBuilder(LogEntry.EventType.VOLUNTEERS_MET, new Date())
                .withCharity(charityToMark)
                .build());
    }

    private void markCharityVolunteersNotMet(Charity charityToMark) {
        Optional<LogEntry> logData = logEntryRepository.findByCharityIdAndEventType(charityToMark.getId(), LogEntry.EventType.VOLUNTEERS_MET);
        logData.ifPresent(logEntry -> logEntryRepository.deleteById(logEntry.getId()));
    }

    @Override
    public ResponseEntity<HttpStatus> deleteCharity(long userId, long charityId) {
        try {
            Optional<User> userData = userRepository.findById(userId);
            Optional<Charity> charityData = charityRepository.findById(charityId);
            if (userData.isPresent() && charityData.isPresent()) {
                if (isUserOwnerOfCharity(userId, charityId)) {
                    deleteLogsForCharity(charityId);
                    charityRepository.deleteById(charityId);
                    logEntryRepository.save(new LogEntry.LogEntryBuilder(LogEntry.EventType.DELETE_CHARITY, new Date())
                            .withDeletedCharity(charityData.get().getName())
                            .withUser(userData.get())
                            .build());
                    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
                } else {
                    return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
                }
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
        }
    }

    private void deleteLogsForCharity(long charityId) {
        List<LogEntry> logsForCharity = logEntryRepository.findByCharityId(charityId);
        for (LogEntry log : logsForCharity) {
            if (log.getEventType() == LogEntry.EventType.VOLUNTEERED_IN_CHARITY ||
                    log.getEventType() == LogEntry.EventType.DONATE_TO_CHARITY) {
                updateUserForCharityDeletion(log);
            }
            log.setDeletedCharity(log.getCharity().getName());
            log.setCharity(null);
            logEntryRepository.save(log);
        }
    }

    @Override
    public ResponseEntity<User> getCharityCreator(long id) {
        Optional<LogEntry> creationLog = logEntryRepository.findByCharityIdAndEventType(id, LogEntry.EventType.CREATE_CHARITY);
        if (creationLog.isPresent()) {
            Optional<User> creatorData = userRepository.findById(creationLog.get().getUser().getId());
            if (creatorData.isPresent()) {
                return new ResponseEntity<>(creatorData.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
        }
    }

    @Override
    public ResponseEntity<List<Charity>> searchForCharity(String name) {
        try {
            List<Charity> charities = new ArrayList<>(charityRepository.findByNameIgnoreCaseContaining(name));
            if (charities.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(charities, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private boolean isUserOwnerOfCharity(long userId, long charityId) {
        Optional<LogEntry> creationLog = logEntryRepository.findByUserIdAndCharityIdAndEventType(userId, charityId,
                LogEntry.EventType.CREATE_CHARITY);
        return creationLog.isPresent();
    }

    private void updateUserForCharityDeletion(LogEntry log) {
        Optional<User> userData = userRepository.findById(log.getUser().getId());
        if (userData.isPresent()) {
            User userToUpdate = userData.get();
            if (log.getEventType() == LogEntry.EventType.DONATE_TO_CHARITY) {
                userToUpdate.setTotalAmountDonated(userToUpdate.getTotalAmountDonated() - log.getMoneyDonated());
                userToUpdate.setTotalDonationsMade(userToUpdate.getTotalDonationsMade() - 1);
                logEntryRepository.save(new LogEntry.LogEntryBuilder(LogEntry.EventType.MONEY_REFUND_DELETE, new Date())
                        .withDeletedCharity(log.getCharity().getName())
                        .withMoneyDonated(log.getMoneyDonated())
                        .withUser(userToUpdate)
                        .build());
            }
            if (log.getEventType() == LogEntry.EventType.VOLUNTEERED_IN_CHARITY) {
                userToUpdate.setTotalVolunteerings(userToUpdate.getTotalVolunteerings() - 1);
                logEntryRepository.save(new LogEntry.LogEntryBuilder(LogEntry.EventType.REMOVED_FROM_CHARITY_DELETE, new Date())
                        .withDeletedCharity(log.getCharity().getName())
                        .withUser(userToUpdate)
                        .build());
            }
            userRepository.save(userToUpdate);

        }
    }

    @Override
    public ResponseEntity<List<User>> getDonators(long id) {
        List<LogEntry> donations = logEntryRepository.findUserDistinctByEventType(LogEntry.EventType.DONATE_TO_CHARITY);
        Set<User> donatorsToCurrentCharity = new HashSet<>();
        for (LogEntry donation : donations) {
            if (donation.getCharity().getId() == id) {
                donatorsToCurrentCharity.add(donation.getUser());
            }
        }
        if (donatorsToCurrentCharity.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(new ArrayList<>(donatorsToCurrentCharity), HttpStatus.OK);
        }
    }

    @Override
    public ResponseEntity<List<User>> getVolunteers(long id) {
        List<LogEntry> volunteerings = logEntryRepository.findUserDistinctByEventType(LogEntry.EventType.VOLUNTEERED_IN_CHARITY);
        Set<User> volunteersInCurrentCharity = new HashSet<>();
        for (LogEntry volunteering : volunteerings) {
            if (volunteering.getCharity().getId() == id) {
                volunteersInCurrentCharity.add(volunteering.getUser());
            }
        }
        if (volunteersInCurrentCharity.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(new ArrayList<>(volunteersInCurrentCharity), HttpStatus.OK);
        }
    }

    @Override
    public ResponseEntity<Double> getMoneyNeeded(long id) {
        Optional<Charity> charityData = charityRepository.findById(id);
        if (charityData.isPresent()) {
            Charity charity = charityData.get();
            Double moneyNeeded = charity.getMoneyRequired() - charity.getMoneyRaised();
            return new ResponseEntity<>(moneyNeeded, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<Integer> getParticipantsNeeded(long id) {
        Optional<Charity> charityData = charityRepository.findById(id);
        if (charityData.isPresent()) {
            Charity charity = charityData.get();
            if (charity.isCharityWithVolunteers()) {
                Integer participantsNeeded = charity.getVolunteersRequired() - charity.getVolunteersGathered();
                return new ResponseEntity<>(participantsNeeded, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}