package com.vmware.finaltask.networkofgiving.manager;

import com.vmware.finaltask.networkofgiving.model.User;
import com.vmware.finaltask.networkofgiving.model.UserLoginDummy;
import com.vmware.finaltask.networkofgiving.operations.LoginOperations;
import com.vmware.finaltask.networkofgiving.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class LoginManager implements LoginOperations {

    @Autowired
    UserRepository userRepository;

    @Override
    public ResponseEntity<User> login(UserLoginDummy dummy) {
        Optional<User> userData = userRepository.findByUsernameAndPass(dummy.getUsername(), dummy.getPassword());
        if (userData.isPresent()) {
            return new ResponseEntity<>(userData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
