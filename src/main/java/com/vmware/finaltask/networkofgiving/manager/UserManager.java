package com.vmware.finaltask.networkofgiving.manager;

import com.vmware.finaltask.networkofgiving.model.Charity;
import com.vmware.finaltask.networkofgiving.model.LogEntry;
import com.vmware.finaltask.networkofgiving.model.User;
import com.vmware.finaltask.networkofgiving.operations.UserOperations;
import com.vmware.finaltask.networkofgiving.repository.CharityRepository;
import com.vmware.finaltask.networkofgiving.repository.LogEntryRepository;
import com.vmware.finaltask.networkofgiving.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class UserManager implements UserOperations {

    @Autowired
    UserRepository userRepository;

    @Autowired
    CharityRepository charityRepository;

    @Autowired
    LogEntryRepository logEntryRepository;

    @Override
    public ResponseEntity<List<User>> getAllUsers() {
        try {
            List<User> users = new ArrayList<>(userRepository.findAll());
            if (users.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(users, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<User> getUserById(long id) {
        Optional<User> userData = userRepository.findById(id);
        if (userData.isPresent()) {
            return new ResponseEntity<>(userData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<User> getUserByUsername(String username) {
        Optional<User> userData = userRepository.findByUsername(username);
        if (userData.isPresent()) {
            return new ResponseEntity<>(userData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<User> createUser(User user) {
        try {
            User addedUser = userRepository.save(new User(user.getFullName(), user.getUsername(), user.getPass(),
                    user.getLocation(), user.getSex(), user.getAge()));
            logEntryRepository.save(new LogEntry.LogEntryBuilder(LogEntry.EventType.CREATE_USER, new Date()).withUser(addedUser).build());
            return new ResponseEntity<>(addedUser, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @Override
    public ResponseEntity<User> updateUser(long id, User givenUser) {
        Optional<User> existingUserData = userRepository.findById(id);
        if (existingUserData.isPresent()) {
            User userToUpdate = existingUserData.get();
            try {
                updateUser(userToUpdate, givenUser);
            } catch (Exception e) {
                return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
            }
            logEntryRepository.save(new LogEntry.LogEntryBuilder(LogEntry.EventType.UPDATE_USER, new Date())
                    .withUser(userToUpdate)
                    .build());
            return new ResponseEntity<>(userToUpdate, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<HttpStatus> deleteUser(long id) {
        Optional<User> userData = userRepository.findById(id);
        if (userData.isPresent()) {
            deleteUserLogs(id);
            userRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    private void deleteUserLogs(long id) {
        List<LogEntry> logsByUser = logEntryRepository.findByUserId(id);
        logEntryRepository.deleteAll(logsByUser);
        for (LogEntry logs : logsByUser) {
            if (logs.getCharity() != null) {
                charityRepository.deleteById(logs.getCharity().getId());
            }
        }
    }

    @Override
    public ResponseEntity<List<Charity>> getCreatedCharities(long id) {
        List<LogEntry> creationLogs = logEntryRepository.findByUserIdAndEventType(id, LogEntry.EventType.CREATE_CHARITY);
        if (creationLogs.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            List<Charity> createdCharities = new ArrayList<>();
            for (LogEntry creationLog : creationLogs) {
                if (creationLog.getCharity() != null) {
                    createdCharities.add(creationLog.getCharity());
                }
            }
            return new ResponseEntity<>(createdCharities, HttpStatus.OK);
        }
    }

    private void updateUser(User userToUpdate, User givenUser) {
        userToUpdate.setFullName(givenUser.getFullName());
        userToUpdate.setUsername(givenUser.getUsername());
        userToUpdate.setPass(givenUser.getPass());
        userToUpdate.setLocation(givenUser.getLocation());
        userToUpdate.setSex(givenUser.getSex());
        userToUpdate.setAge(givenUser.getAge());
        userRepository.save(userToUpdate);
    }

    @Override
    public ResponseEntity<Charity> donateMoney(long userId, long charityId, Double donation) {
        Optional<User> userData = userRepository.findById(userId);
        Optional<Charity> charityData = charityRepository.findById(charityId);
        if (userData.isPresent() && charityData.isPresent()) {
            Charity charityToBeDonatedTo = charityData.get();
            User donatedUser = userData.get();
            if (charityToBeDonatedTo.isCharityWithMoney() &&
                    charityToBeDonatedTo.isReceivingValidDonation(donation)) {
                charityToBeDonatedTo.setMoneyRaised(charityToBeDonatedTo.getMoneyRaised() + donation);
                updateDonatedUser(donatedUser, donation);
                charityRepository.save(charityToBeDonatedTo);
                logEntryRepository.save(new LogEntry.LogEntryBuilder(LogEntry.EventType.DONATE_TO_CHARITY, new Date())
                        .withCharity(charityToBeDonatedTo)
                        .withUser(donatedUser)
                        .withMoneyDonated(donation)
                        .build());
                if (charityToBeDonatedTo.getMoneyRaised().equals(charityToBeDonatedTo.getMoneyRequired())) {
                    markCharityMoneyMet(charityToBeDonatedTo);
                }
                return new ResponseEntity<>(charityToBeDonatedTo, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    private void markCharityMoneyMet(Charity charityToMark) {
        logEntryRepository.save(new LogEntry.LogEntryBuilder(LogEntry.EventType.MONEY_MET, new Date())
                .withCharity(charityToMark)
                .build());
    }

    @Override
    public ResponseEntity<Boolean> isVolunteering(long userId, long charityId) {
        Optional<LogEntry> userData = logEntryRepository.findUserByUserIdAndCharityIdAndEventTypeAndVolunteered(userId,
                charityId, LogEntry.EventType.VOLUNTEERED_IN_CHARITY, true);
        if (userData.isPresent()) {
            return new ResponseEntity<>(true, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(false, HttpStatus.NOT_FOUND);
        }
    }

    private void updateDonatedUser(User donatedUser, Double donation) {
        donatedUser.setTotalAmountDonated(donatedUser.getTotalAmountDonated() + donation);
        donatedUser.setTotalDonationsMade(donatedUser.getTotalDonationsMade() + 1);
        userRepository.save(donatedUser);
    }

    @Override
    public ResponseEntity<Charity> volunteerInCharity(long userId, long charityId, Boolean isVolunteering) {
        Optional<User> userData = userRepository.findById(userId);
        Optional<Charity> charityData = charityRepository.findById(charityId);
        if (userData.isPresent() && charityData.isPresent()) {
            Charity updatedCharity = charityData.get();
            User updatedUser = userData.get();
            if (updatedCharity.isCharityWithVolunteers()) {
                if (userIsNotVolunteering(userId, charityId) && updatedCharity.areParticipantsNotEnough()) {
                    updatedCharity.setVolunteersGathered(updatedCharity.getVolunteersGathered() + 1);
                    charityRepository.save(updatedCharity);
                    updatedUser.setTotalVolunteerings(updatedUser.getTotalVolunteerings() + 1);
                    userRepository.save(updatedUser);
                    logEntryRepository.save(new LogEntry.LogEntryBuilder(LogEntry.EventType.VOLUNTEERED_IN_CHARITY, new Date())
                            .withCharity(updatedCharity)
                            .withUser(updatedUser)
                            .withVolunteered(isVolunteering)
                            .build());
                    if (updatedCharity.getVolunteersGathered().equals(updatedCharity.getVolunteersRequired())) {
                        markCharityVolunteersMet(updatedCharity);
                    }
                    return new ResponseEntity<>(updatedCharity, HttpStatus.OK);
                } else {
                    return new ResponseEntity<>(HttpStatus.CONFLICT);
                }
            } else {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<List<LogEntry>> getEventsByUser(long userId) {
        Set<LogEntry> logs = new TreeSet<>(logEntryRepository.findByUserIdOrderByEventTimeDesc(userId));
        Set<Charity> charitiesThatUserIsInvolvedIn = new HashSet<>();
        for (LogEntry log : logs) {
            if (log.getCharity() != null) {
                charitiesThatUserIsInvolvedIn.add(log.getCharity());
            }
        }
        for (Charity charity : charitiesThatUserIsInvolvedIn) {
            Optional<LogEntry> logForMoneyCompletedCharityThatUserIsInvolvedIn = logEntryRepository
                    .findByCharityIdAndEventType(charity.getId(), LogEntry.EventType.MONEY_MET);
            logForMoneyCompletedCharityThatUserIsInvolvedIn.ifPresent(logs::add);
            Optional<LogEntry> logForVolunteersCompletedCharityThatUserIsInvolvedIn = logEntryRepository.
                    findByCharityIdAndEventType(charity.getId(), LogEntry.EventType.VOLUNTEERS_MET);
            logForVolunteersCompletedCharityThatUserIsInvolvedIn.ifPresent(logs::add);
        }
        if (logs.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(new ArrayList<>(logs), HttpStatus.OK);
        }
    }

    private void markCharityVolunteersMet(Charity charityToMark) {
        logEntryRepository.save(new LogEntry.LogEntryBuilder(LogEntry.EventType.VOLUNTEERS_MET, new Date())
                .withCharity(charityToMark)
                .build());
    }


    private boolean userIsNotVolunteering(long userId, long charityId) {
        Optional<LogEntry> userData = logEntryRepository.findUserByUserIdAndCharityIdAndEventTypeAndVolunteered(userId,
                charityId, LogEntry.EventType.VOLUNTEERED_IN_CHARITY, true);
        return userData.isEmpty();
    }
}
