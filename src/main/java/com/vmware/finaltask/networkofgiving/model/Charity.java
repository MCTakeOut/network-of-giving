package com.vmware.finaltask.networkofgiving.model;

import javax.persistence.*;

@Entity
@Table(name = "charities", schema = "mhristov")
public class Charity {

    private static final Double DONATED_MONEY_DEFAULT = 0.0;
    private static final Integer NUMBER_OF_PARTICIPANTS_DEFAULT = 0;

    public enum CharityType {
        MONEY_CHARITY,
        VOLUNTEER_CHARITY,
        MONEY_AND_VOLUNTEERS_CHARITY
    }

    public static class CharityBuilder {
        private String name;
        private String thumbnailName;
        private String description;
        private CharityType charityType;
        private Double moneyRequired;
        private Double moneyRaised;
        private Integer volunteersRequired;
        private Integer volunteersGathered;

        public CharityBuilder(String name, String thumbnailName, String description, CharityType charityType) {
            this.name = name;
            this.thumbnailName = thumbnailName;
            this.description = description;
            this.charityType = charityType;
        }

        public CharityBuilder withMoney(Double money) {
            this.moneyRequired = money;
            return this;
        }

        public CharityBuilder withVolunteers(Integer volunteers) {
            this.volunteersRequired = volunteers;
            return this;
        }

        public Charity build() {
            Charity charity = new Charity();
            charity.name = this.name;
            charity.thumbnailName = this.thumbnailName;
            charity.description = this.description;
            charity.charityType = this.charityType;
            if (this.moneyRequired != null) {
                charity.moneyRequired = this.moneyRequired;
                charity.moneyRaised = DONATED_MONEY_DEFAULT;
            } else {
                charity.moneyRequired = null;
                charity.moneyRaised = null;
            }

            if (this.volunteersRequired != null) {
                charity.volunteersRequired = this.volunteersRequired;
                charity.volunteersGathered = NUMBER_OF_PARTICIPANTS_DEFAULT;
            } else {
                charity.volunteersRequired = null;
                charity.volunteersGathered = null;
            }
            return charity;
        }
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "name", unique = true)
    private String name;

    @Column(name = "thumbnailName")
    private String thumbnailName;

    @Column(name = "description", columnDefinition="TEXT")
    private String description;

    @Column(name = "charityType")
    private CharityType charityType;

    @Column(name = "moneyRequired")
    private Double moneyRequired;

    @Column(name = "moneyRaised")
    private Double moneyRaised;

    @Column(name = "volunteersRequired")
    private Integer volunteersRequired;

    @Column(name = "volunteersGathered")
    private Integer volunteersGathered;

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getThumbnailName() {
        return thumbnailName;
    }

    public void setThumbnailName(String thumbnailName) {
        this.thumbnailName = thumbnailName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getMoneyRequired() {
        return moneyRequired;
    }

    public void setMoneyRequired(Double moneyRequired) {
        this.moneyRequired = moneyRequired;
    }

    public Double getMoneyRaised() {
        return moneyRaised;
    }

    public void setMoneyRaised(Double moneyRaised) {
        this.moneyRaised = moneyRaised;
    }

    public Integer getVolunteersRequired() {
        return volunteersRequired;
    }

    public void setVolunteersRequired(Integer volunteersRequired) {
        this.volunteersRequired = volunteersRequired;
    }

    public Integer getVolunteersGathered() {
        return volunteersGathered;
    }

    public void setVolunteersGathered(Integer volunteersGathered) {
        this.volunteersGathered = volunteersGathered;
    }

    public CharityType getCharityType() {
        return charityType;
    }

    public void setCharityType(CharityType charityType) {
        this.charityType = charityType;
    }

    public boolean isCharityWithMoney() {
        return this.charityType == CharityType.MONEY_CHARITY ||
                this.charityType == CharityType.MONEY_AND_VOLUNTEERS_CHARITY;
    }

    public boolean isCharityWithVolunteers() {
        return this.charityType == CharityType.VOLUNTEER_CHARITY ||
                this.charityType == CharityType.MONEY_AND_VOLUNTEERS_CHARITY;
    }

    public boolean isReceivingValidDonation(Double donation) {
        return (donation > 0) && (this.moneyRaised + donation) <= moneyRequired;
    }

    public boolean areParticipantsNotEnough() {
        return this.volunteersGathered < this.volunteersRequired;
    }
}
