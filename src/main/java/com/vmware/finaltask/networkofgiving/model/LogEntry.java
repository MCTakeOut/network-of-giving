package com.vmware.finaltask.networkofgiving.model;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "eventLog", schema = "mhristov")
public class LogEntry implements Comparable<LogEntry>{

    public enum EventType {
        CREATE_CHARITY,
        CREATE_USER,
        DONATE_TO_CHARITY,
        VOLUNTEERED_IN_CHARITY,
        UPDATE_USER,
        UPDATE_CHARITY,
        DELETE_CHARITY,
        REMOVED_FROM_CHARITY_UPDATE,
        REMOVED_FROM_CHARITY_DELETE,
        MONEY_REFUND_UPDATE,
        MONEY_REFUND_DELETE,
        VOLUNTEERS_MET,
        MONEY_MET
    }

    public static class LogEntryBuilder {
        private User user;
        private EventType eventType;
        private String deletedCharity;
        private Charity charity;
        private Date eventTime;
        private Double moneyDonated;
        private Boolean volunteered;

        public LogEntryBuilder(EventType eventType, Date eventTime) {
            this.eventType = eventType;
            this.eventTime = eventTime;
        }

        public LogEntryBuilder withDeletedCharity(String deletedCharity) {
            this.deletedCharity = deletedCharity;
            return this;
        }

        public LogEntryBuilder withCharity(Charity charity) {
            this.charity = charity;
            return this;
        }

        public LogEntryBuilder withUser(User user) {
            this.user = user;
            return this;
        }

        public LogEntryBuilder withMoneyDonated(Double moneyDonated) {
            this.moneyDonated = moneyDonated;
            return this;
        }

        public LogEntryBuilder withVolunteered(Boolean volunteered) {
            this.volunteered = volunteered;
            return this;
        }

        public LogEntry build() {
            LogEntry logEntry = new LogEntry();
            if (this.user != null) {
                logEntry.user = this.user;
            } else {
                logEntry.user = null;
            }
            if (this.charity != null) {
                logEntry.charity = this.charity;
            } else {
                logEntry.charity = null;
            }
            if (this.deletedCharity != null) {
                logEntry.deletedCharity = this.deletedCharity;
            } else {
                logEntry.deletedCharity = null;
            }
            logEntry.eventType = this.eventType;
            logEntry.eventTime = this.eventTime;
            if (this.moneyDonated != null) {
                logEntry.moneyDonated = this.moneyDonated;
            } else {
                logEntry.moneyDonated = null;
            }
            if (this.volunteered != null) {
                logEntry.volunteered = this.volunteered;
            } else {
                logEntry.volunteered = null;
            }
            return logEntry;
        }
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @OnDelete(action = OnDeleteAction.NO_ACTION)
    @OneToOne()
    @JoinColumn(name = "userId", referencedColumnName = "id")
    private User user;

    @Column(name = "eventType")
    private EventType eventType;

    @Column(name = "deletedCharity")
    private String deletedCharity;

    @OnDelete(action = OnDeleteAction.NO_ACTION)
    @OneToOne()
    @JoinColumn(name = "charityId", referencedColumnName = "id")
    private Charity charity;

    @Basic(optional = false)
    @Column(name = "eventTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date eventTime;

    @Column(name = "moneyDonated")
    private Double moneyDonated;

    @Column(name = "volunteered")
    private Boolean volunteered;


    private LogEntry() {
    }

    public long getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    public String getDeletedCharity() {
        return deletedCharity;
    }

    public void setDeletedCharity(String deletedCharity) {
        this.deletedCharity = deletedCharity;
    }

    public Charity getCharity() {
        return charity;
    }

    public void setCharity(Charity charity) {
        this.charity = charity;
    }

    public Date getEventTime() {
        return eventTime;
    }

    public void setEventTime(Date eventTime) {
        this.eventTime = eventTime;
    }

    public Double getMoneyDonated() {
        return moneyDonated;
    }

    public void setMoneyDonated(Double moneyDonated) {
        this.moneyDonated = moneyDonated;
    }

    public Boolean getVolunteered() {
        return volunteered;
    }

    public void setVolunteered(Boolean volunteered) {
        this.volunteered = volunteered;
    }

    @Override
    public int compareTo(LogEntry log) {
        if (getEventTime() == null || log.getEventTime() == null) {
            return 0;
        }
        return log.getEventTime().compareTo(getEventTime());
    }
}

