package com.vmware.finaltask.networkofgiving.model;


import javax.persistence.*;

@Entity
@Table(name = "users", schema = "mhristov")
public class User {

    private static final Double DEFAULT_TOTAL_AMOUNT_DONATED = 0.0;
    private static final Integer DEFAULT_TOTAL_DONATIONS = 0;
    private static final Integer DEFAULT_TOTAL_PARTICIPATIONS = 0;

    public enum Gender {
        MALE, FEMALE, OTHER
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "fullName")
    private String fullName;

    @Column(name = "username", unique = true)
    private String username;

    @Column(name = "pass")
    private String pass;

    @Column(name = "location")
    private String location;

    @Column(name = "sex")
    private Gender sex;

    @Column(name = "age")
    private Integer age;

    @Column(name = "totalAmountDonated")
    private Double totalAmountDonated;

    @Column(name = "totalDonationsMade")
    private Integer totalDonationsMade;

    @Column(name = "totalVolunteerings")
    private Integer totalVolunteerings;

    public User() {
    }

    public User(String fullName, String username, String pass, String location, Gender sex, int age) {
        this.fullName = fullName;
        this.username = username;
        this.pass = pass;
        this.location = location;
        this.sex = sex;
        this.age = age;
        this.totalAmountDonated = DEFAULT_TOTAL_AMOUNT_DONATED;
        this.totalDonationsMade = DEFAULT_TOTAL_DONATIONS;
        this.totalVolunteerings = DEFAULT_TOTAL_PARTICIPATIONS;
    }


    public long getId() {
        return id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Gender getSex() {
        return sex;
    }

    public void setSex(Gender sex) {
        this.sex = sex;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Double getTotalAmountDonated() {
        return totalAmountDonated;
    }

    public void setTotalAmountDonated(Double totalAmountDonated) {
        this.totalAmountDonated = totalAmountDonated;
    }

    public Integer getTotalDonationsMade() {
        return totalDonationsMade;
    }

    public void setTotalDonationsMade(Integer totalDonationsMade) {
        this.totalDonationsMade = totalDonationsMade;
    }

    public Integer getTotalVolunteerings() {
        return totalVolunteerings;
    }

    public void setTotalVolunteerings(Integer totalVolunteerings) {
        this.totalVolunteerings = totalVolunteerings;
    }
}

