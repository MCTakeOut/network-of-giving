package com.vmware.finaltask.networkofgiving.operations;

import com.vmware.finaltask.networkofgiving.model.Charity;
import com.vmware.finaltask.networkofgiving.model.User;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/default")
public interface ChraityOperations {

    @GetMapping("/charity")
    public ResponseEntity<List<Charity>> getAllCharities();

    @GetMapping(value = "/charity/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Charity> getCharityById(@PathVariable("id") long id);

    @GetMapping(value = "/charity/find")
    public ResponseEntity<Charity> getCharityByName(@RequestParam String name);

    @PostMapping(value = "/user/{id}/charity", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Charity> createCharity(@PathVariable long id, @RequestBody Charity charity);

    @PutMapping(value = "/user/{userId}/charity/{charityId}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Charity> updateCharity(@PathVariable long userId, @PathVariable long charityId,
                                                 @RequestBody Charity charity);

    @DeleteMapping("/user/{userId}/charity/{charityId}")
    public ResponseEntity<HttpStatus> deleteCharity(@PathVariable long userId, @PathVariable long charityId);

    @GetMapping("/charity/get-creator/{id}")
    public ResponseEntity<User> getCharityCreator(@PathVariable long id);

    @GetMapping("/charity/search")
    public ResponseEntity<List<Charity>> searchForCharity(@RequestParam String name);

    @GetMapping("/charity/donators/{id}")
    public ResponseEntity<List<User>> getDonators(@PathVariable long id);

    @GetMapping("charity/participants/{id}")
    public ResponseEntity<List<User>> getVolunteers(@PathVariable long id);

    @GetMapping("charity/money-needed/{id}")
    public ResponseEntity<Double> getMoneyNeeded(@PathVariable long id);

    @GetMapping("charity/participants-needed/{id}")
    public ResponseEntity<Integer> getParticipantsNeeded(@PathVariable long id);

}
