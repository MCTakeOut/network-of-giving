package com.vmware.finaltask.networkofgiving.operations;

import com.vmware.finaltask.networkofgiving.model.User;
import com.vmware.finaltask.networkofgiving.model.UserLoginDummy;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/default")
public interface LoginOperations {
    @PostMapping("/login")
    public ResponseEntity<User> login(@RequestBody UserLoginDummy dummy);
}

