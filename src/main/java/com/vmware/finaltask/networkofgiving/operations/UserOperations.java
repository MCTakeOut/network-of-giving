package com.vmware.finaltask.networkofgiving.operations;


import com.vmware.finaltask.networkofgiving.model.Charity;
import com.vmware.finaltask.networkofgiving.model.LogEntry;
import com.vmware.finaltask.networkofgiving.model.User;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/default")
public interface UserOperations {

    @GetMapping("/user")
    public ResponseEntity<List<User>> getAllUsers();

    @GetMapping(value = "/user/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> getUserById(@PathVariable("id") long id);

    @PostMapping(value = "/user", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> createUser(@RequestBody User user);

    @PutMapping(value = "/user/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> updateUser(@PathVariable("id") long id, @RequestBody User user);

    @DeleteMapping("/user/delete/{id}")
    public ResponseEntity<HttpStatus> deleteUser(@PathVariable("id") long id);

    @GetMapping("/user/{id}/created-charities")
    public ResponseEntity<List<Charity>> getCreatedCharities(@PathVariable long id);

    @GetMapping(value = "/user/find")
    public ResponseEntity<User> getUserByUsername(@RequestParam String username);

    @PutMapping(value = "/user/{userId}/charity/{charityId}/donate")
    public ResponseEntity<Charity> donateMoney(@PathVariable long userId, @PathVariable long charityId,
                                               @RequestParam Double donation);
    @GetMapping(value = "/user/{userId}/chaity/{charityId}/isVolunteering")
    public ResponseEntity<Boolean> isVolunteering(@PathVariable long userId, @PathVariable long charityId);

    @PutMapping(value = "/user/{userId}/charity/{charityId}/volunteer")
    public ResponseEntity<Charity> volunteerInCharity(@PathVariable long userId, @PathVariable long charityId,
                                                      @RequestParam Boolean isVolunteering);
    @GetMapping(value = "/user/{userId}/events")
    public ResponseEntity<List<LogEntry>> getEventsByUser(@PathVariable long userId);
}
