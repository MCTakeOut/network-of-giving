package com.vmware.finaltask.networkofgiving.repository;

import com.vmware.finaltask.networkofgiving.model.Charity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CharityRepository extends JpaRepository<Charity, Long> {
    Optional<Charity> findByName(String name);
    List<Charity> findByNameIgnoreCaseContaining(String name);

}

