package com.vmware.finaltask.networkofgiving.repository;

import com.vmware.finaltask.networkofgiving.model.LogEntry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface LogEntryRepository extends JpaRepository<LogEntry, Long> {
    List<LogEntry> findByUserId(long id);

    List<LogEntry> findByUserIdOrderByEventTimeDesc(long id);

    List<LogEntry> findUserDistinctByEventType(LogEntry.EventType eventType);

    Optional<LogEntry> findUserByUserIdAndCharityIdAndEventTypeAndVolunteered(long userid, long charityId,
                                                                              LogEntry.EventType eventType, Boolean volunteered);

    Optional<LogEntry> findByUserIdAndCharityIdAndEventType(long userId, long charityId, LogEntry.EventType eventType);

    List<LogEntry> findByCharityIdAndEventTypeOrderByEventTimeDesc(long id, LogEntry.EventType eventType);

    List<LogEntry> findByCharityId(long id);

    List<LogEntry> findByCharityIdOrderByEventTimeDesc(long id);

    List<LogEntry> findByUserIdAndEventType(long id, LogEntry.EventType eventType);

    Optional<LogEntry> findByCharityIdAndEventType(long id, LogEntry.EventType eventType);
}