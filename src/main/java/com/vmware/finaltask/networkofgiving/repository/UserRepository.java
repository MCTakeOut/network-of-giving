package com.vmware.finaltask.networkofgiving.repository;

import com.vmware.finaltask.networkofgiving.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);
    Optional<User> findByUsernameAndPass(String username, String pass);
}
