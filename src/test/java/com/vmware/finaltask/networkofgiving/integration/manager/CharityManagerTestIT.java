package com.vmware.finaltask.networkofgiving.integration.manager;

import com.vmware.finaltask.networkofgiving.model.Charity;
import com.vmware.finaltask.networkofgiving.model.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Objects;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CharityManagerTestIT {

    private static Charity charity1;
    private static Charity charity2;
    private static Charity charity3;
    private static User user1;
    private static User user2;
    private static User user3;

    @Autowired
    private TestRestTemplate restTemplate;

    private static final int port = 8080;

    @Before
    public void init() {
        charity1 = new Charity.CharityBuilder("charity1", "thumbnail1", "description1",
                Charity.CharityType.MONEY_CHARITY).withMoney(100.0).build();
        charity2 = new Charity.CharityBuilder("charity2", "thumbnail2", "description2",
                Charity.CharityType.VOLUNTEER_CHARITY).withVolunteers(200).build();
        charity3 = new Charity.CharityBuilder("charity3", "thumbnail3", "description3",
                Charity.CharityType.MONEY_AND_VOLUNTEERS_CHARITY).withMoney(300.0).withVolunteers(30).build();
        user1 = new User("user1", "userName1", "pass1", "city1", User.Gender.MALE, 1);
        user2 = new User("user2", "userName2", "pass2", "city2", User.Gender.FEMALE, 2);
        user3 = new User("user3", "userName3", "pass3", "city3", User.Gender.OTHER, 3);
    }

    @Test
    public void findAllCharitiesIT() {
        HttpEntity<User> requestUser1 = new HttpEntity<>(user1);
        ResponseEntity<User> responseUser1 = restTemplate.postForEntity("http://localhost:" + port + "/api/user/", requestUser1, User.class);
        HttpEntity<Charity> requestCharity1 = new HttpEntity<>(charity1);
        long creatorId1 = Objects.requireNonNull(responseUser1.getBody()).getId();
        ResponseEntity<Charity> responseCharity1 = restTemplate.postForEntity("http://localhost:" + port + "/api/user/" + creatorId1 + "/charity", requestCharity1, Charity.class);
        HttpEntity<User> requestUser2 = new HttpEntity<>(user2);
        ResponseEntity<User> responseUser2 = restTemplate.postForEntity("http://localhost:" + port + "/api/user/", requestUser2, User.class);
        HttpEntity<Charity> requestCharity2 = new HttpEntity<>(charity2);
        long creatorId2 = Objects.requireNonNull(responseUser2.getBody()).getId();
        ResponseEntity<Charity> responseCharity2 = restTemplate.postForEntity("http://localhost:" + port + "/api/user/" + creatorId2 + "/charity", requestCharity2, Charity.class);
        HttpEntity<User> requestUser3 = new HttpEntity<>(user3);
        ResponseEntity<User> responseUser3 = restTemplate.postForEntity("http://localhost:" + port + "/api/user/", requestUser3, User.class);
        HttpEntity<Charity> requestCharity3 = new HttpEntity<>(charity3);
        long creatorId3 = Objects.requireNonNull(responseUser3.getBody()).getId();
        ResponseEntity<Charity> responseCharity3 = restTemplate.postForEntity("http://localhost:" + port + "/api/user/" + creatorId3 + "/charity", requestCharity3, Charity.class);
        ResponseEntity<Charity> resultCharity1 = this.restTemplate.getForEntity("http://localhost:" + port + "/api/charity/" + Objects.requireNonNull(responseCharity1.getBody()).getId(), Charity.class);
        ResponseEntity<Charity> resultCharity2 = this.restTemplate.getForEntity("http://localhost:" + port + "/api/charity/" + Objects.requireNonNull(responseCharity2.getBody()).getId(), Charity.class);
        ResponseEntity<Charity> resultCharity3 = this.restTemplate.getForEntity("http://localhost:" + port + "/api/charity/" + Objects.requireNonNull(responseCharity3.getBody()).getId(), Charity.class);
        assertThat(resultCharity1.getStatusCode(), is(HttpStatus.OK));
        assertThat(resultCharity2.getStatusCode(), is(HttpStatus.OK));
        assertThat(resultCharity3.getStatusCode(), is(HttpStatus.OK));
        assertThat(resultCharity1.getBody(), is(notNullValue()));
        assertThat(resultCharity2.getBody(), is(notNullValue()));
        assertThat(resultCharity3.getBody(), is(notNullValue()));
        assertThat(resultCharity1.getBody().getName(), is(charity1.getName()));
        assertThat(resultCharity2.getBody().getName(), is(charity2.getName()));
        assertThat(resultCharity3.getBody().getName(), is(charity3.getName()));
        restTemplate.delete("http://localhost:" + port + "/api/user/delete/" + creatorId1);
        restTemplate.delete("http://localhost:" + port + "/api/user/delete/" + creatorId2);
        restTemplate.delete("http://localhost:" + port + "/api/user/delete/" + creatorId3);
    }

    @Test
    public void createCharityTestIT() {
        HttpEntity<User> requestUser = new HttpEntity<>(user1);
        ResponseEntity<User> responseUser = restTemplate.postForEntity("http://localhost:" + port + "/api/user/", requestUser, User.class);
        HttpEntity<Charity> requestCharity = new HttpEntity<>(charity3);
        long creatorId = Objects.requireNonNull(responseUser.getBody()).getId();
        ResponseEntity<Charity> responseCharity = restTemplate.postForEntity("http://localhost:" + port + "/api/user/" + creatorId + "/charity", requestCharity, Charity.class);
        assertThat(responseCharity.getStatusCode(), is(HttpStatus.OK));
        assertThat(Objects.requireNonNull(responseCharity.getBody()).getName(), is(charity3.getName()));
        restTemplate.delete("http://localhost:" + port + "/api/user/delete/" + creatorId);
    }

    @Test
    public void findByIDTestIT() {
        HttpEntity<User> requestUser = new HttpEntity<>(user2);
        ResponseEntity<User> responseUser = restTemplate.postForEntity("http://localhost:" + port + "/api/user/", requestUser, User.class);
        HttpEntity<Charity> requestCharity = new HttpEntity<>(charity2);
        long creatorId = Objects.requireNonNull(responseUser.getBody()).getId();
        ResponseEntity<Charity> responseCharity = restTemplate.postForEntity("http://localhost:" + port + "/api/user/" + creatorId + "/charity", requestCharity, Charity.class);
        long charityId = Objects.requireNonNull(responseCharity.getBody()).getId();
        responseCharity = restTemplate.getForEntity("http://localhost:" + port + "/api/charity/" + charityId, Charity.class);
        assertThat(Objects.requireNonNull(responseCharity.getBody()).getId(), is(charityId));
        assertThat(responseCharity.getBody().getName(), is(charity2.getName()));
        restTemplate.delete("http://localhost:" + port + "/api/user/+ " + creatorId + "/charity/" + responseCharity.getBody().getId());
        restTemplate.delete("http://localhost:" + port + "/api/user/delete/" + creatorId);
    }

    @Test
    public void deleteByIDTestIT() {
        HttpEntity<Charity> request = new HttpEntity<>(charity1);
        ResponseEntity<Charity> response = restTemplate.postForEntity("http://localhost:" + port + "/api/charity/", request, Charity.class);
        long id = Objects.requireNonNull(response.getBody()).getId();
        restTemplate.delete("http://localhost:" + port + "/api/user/1/charity/" + id);
        response = restTemplate.getForEntity("http://localhost:" + port + "/api/charity/" + id, Charity.class);
        assertThat(response.getBody(), is(nullValue()));
    }
}
