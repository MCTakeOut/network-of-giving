package com.vmware.finaltask.networkofgiving.integration.manager;


import com.vmware.finaltask.networkofgiving.model.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Objects;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserManagerTestIT {
    private static User user1;
    private static User user2;
    private static User user3;

    @Autowired
    private TestRestTemplate restTemplate;

    private static final int port = 8080;

    @Before
    public void init() {
        user1 = new User("user1", "userName1", "pass1", "city1", User.Gender.MALE, 1);
        user2 = new User("user2", "userName2", "pass2", "city2", User.Gender.FEMALE, 2);
        user3 = new User("user3", "userName3", "pass3", "city3", User.Gender.OTHER, 3);
    }


    @Test
    public void findAllUsersIT() {
        HttpEntity<User> request = new HttpEntity<>(user1);
        ResponseEntity<User> response = restTemplate.postForEntity("http://localhost:" + port + "/api/user/", request, User.class);
        ResponseEntity<User[]> result = this.restTemplate.getForEntity("http://localhost:" + port + "/api/user/", User[].class);
        assertThat(result.getStatusCode(), is(HttpStatus.OK));
        assertThat(result.getBody(), is(notNullValue()));
        restTemplate.delete("http://localhost:" + port + "/api/user/delete/" + response.getBody().getId());
    }

    @Test
    public void createUserTestIT() {
        HttpEntity<User> request = new HttpEntity<>(user1);
        ResponseEntity<User> response = restTemplate.postForEntity("http://localhost:" + port + "/api/user/", request, User.class);
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(Objects.requireNonNull(response.getBody()).getFullName(), is(user1.getFullName()));
        restTemplate.delete("http://localhost:" + port + "/api/user/delete/" + response.getBody().getId());
    }

    @Test
    public void findByIDTestIT() {
        HttpEntity<User> request = new HttpEntity<>(user2);
        ResponseEntity<User> response = restTemplate.postForEntity("http://localhost:" + port + "/api/user/", request, User.class);
        long id = Objects.requireNonNull(response.getBody()).getId();
        response = restTemplate.getForEntity("http://localhost:" + port + "/api/user/" + id, User.class);
        assertThat(Objects.requireNonNull(response.getBody()).getId(), is(id));
        assertThat(response.getBody().getFullName(), is(user2.getFullName()));
        restTemplate.delete("http://localhost:" + port + "/api/user/delete/" + id);
    }

    @Test
    public void deleteByIDTestIT() {
        HttpEntity<User> request = new HttpEntity<>(user3);
        ResponseEntity<User> response = restTemplate.postForEntity("http://localhost:" + port + "/api/user/", request, User.class);
        long id = Objects.requireNonNull(response.getBody()).getId();
        restTemplate.delete("http://localhost:" + port + "/api/user/delete/" + id);
        response = restTemplate.getForEntity("http://localhost:" + port + "/api/user/" + id, User.class);
        assertThat(response.getBody(), is(nullValue()));
    }
}

