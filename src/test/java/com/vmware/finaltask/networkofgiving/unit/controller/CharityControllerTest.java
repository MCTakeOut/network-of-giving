package com.vmware.finaltask.networkofgiving.unit.controller;

import com.vmware.finaltask.networkofgiving.controller.CharityController;
import com.vmware.finaltask.networkofgiving.manager.CharityManager;
import com.vmware.finaltask.networkofgiving.model.Charity;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.Objects;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@RunWith(SpringRunner.class)
@ExtendWith(MockitoExtension.class)
@SpringBootTest
public class CharityControllerTest {

    private static Charity charity1;
    private static Charity charity2;
    private static Charity charity3;

    @Mock
    private CharityManager charityManager;

    @InjectMocks
    private CharityController charityController;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Before
    public void init() {
        charity1 = new Charity.CharityBuilder("charity1", "thumbnail1", "description1",
                Charity.CharityType.MONEY_CHARITY).withMoney(100.0).build();
        charity2 = new Charity.CharityBuilder("charity2", "thumbnail2", "description2",
                Charity.CharityType.VOLUNTEER_CHARITY).withVolunteers(200).build();
        charity3 = new Charity.CharityBuilder("charity3", "thumbnail3", "description3",
                Charity.CharityType.MONEY_CHARITY).withMoney(300.0).build();
    }

    @Test
    public void getAllCharitiesWhenNoRecord() {
        Mockito.when(charityManager.getAllCharities()).thenReturn(new ResponseEntity<>(Collections.emptyList(),
                HttpStatus.NO_CONTENT));
        assertThat(Objects.requireNonNull(charityController.getAllCharities().getBody()).size(), is(0));
        Mockito.verify(charityManager, Mockito.times(1)).getAllCharities();
    }

    @Test
    public void getAllCharitiesWhenRecord() {
        Mockito.when(charityManager.getAllCharities()).thenReturn(new ResponseEntity<>(Arrays.asList(charity1, charity2),
                HttpStatus.NO_CONTENT));
        assertThat(Objects.requireNonNull(charityController.getAllCharities().getBody()).size(), is(2));
        Mockito.verify(charityManager, Mockito.times(1)).getAllCharities();
    }

    @Test
    public void createCharity() {
        ResponseEntity<Charity> charity = charityController.createCharity(2L, charity1);
        Mockito.verify(charityManager, Mockito.times(1)).createCharity(2L, charity1);
    }

    @Test
    public void getCharityByIdWhenMatch() {
        Mockito.when(charityManager.getCharityById(1L)).thenReturn(new ResponseEntity<>(charity1, HttpStatus.OK));
        ResponseEntity<Charity> charity = charityController.getCharityById(1L);
        assertThat(charity.getBody(), is(charity1));
        assertThat(charity.getStatusCode(), is(HttpStatus.OK));
    }

    @Test
    public void getCharityByIdWhenNoMatch() {
        Mockito.when(charityManager.getCharityById(1L)).thenReturn(new ResponseEntity<>(HttpStatus.NOT_FOUND));
        ResponseEntity<Charity> charity = charityController.getCharityById(1L);
        assertThat(charity.getStatusCode(), is(HttpStatus.NOT_FOUND));
    }
}

