package com.vmware.finaltask.networkofgiving.unit.controller;


import com.vmware.finaltask.networkofgiving.controller.UserController;
import com.vmware.finaltask.networkofgiving.manager.UserManager;
import com.vmware.finaltask.networkofgiving.model.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.Objects;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@RunWith(SpringRunner.class)
@ExtendWith(MockitoExtension.class)
@SpringBootTest
public class UserControllerTest {
    private static User user1;
    private static User user2;
    private static User user3;

    @Mock
    private UserManager userManager;

    @InjectMocks
    private UserController userController;

    @Before
    public void setUp () {
        MockitoAnnotations.initMocks(this);;
    }

    @Before
    public void init() {
        user1 = new User("user1", "userName1", "pass1", "city1", User.Gender.MALE, 1);
        user2 = new User("user2", "userName2", "pass2", "city2", User.Gender.FEMALE, 2);
        user3 = new User("user3", "userName3", "pass3", "city3", User.Gender.OTHER, 3);
    }

    @Test
    public void getAllUsersWhenNoRecord() {
        Mockito.when(userManager.getAllUsers()).thenReturn(new ResponseEntity<>(Collections.emptyList(), HttpStatus.NO_CONTENT));
        assertThat(Objects.requireNonNull(userController.getAllUsers().getBody()).size(), is(0));
        Mockito.verify(userManager, Mockito.times(1)).getAllUsers();
    }

    @Test
    public void getAllUsersWhenRecord() {
        Mockito.when(userManager.getAllUsers()).thenReturn(new ResponseEntity<>(Arrays.asList(user1, user2), HttpStatus.NO_CONTENT));
        assertThat(Objects.requireNonNull(userController.getAllUsers().getBody()).size(), is(2));
        Mockito.verify(userManager, Mockito.times(1)).getAllUsers();
    }

    @Test
    public void createUser() {
        ResponseEntity<User> user = userController.createUser(user1);
        Mockito.verify(userManager, Mockito.times(1)).createUser(user1);
    }

    @Test
    public void getUserByIdWhenMatch() {
        Mockito.when(userManager.getUserById(1L)).thenReturn(new ResponseEntity<>(user1, HttpStatus.OK));
        ResponseEntity<User> user = userController.getUserById(1L);
        assertThat(user.getBody(), is(user1));
        assertThat(user.getStatusCode(), is(HttpStatus.OK));
    }

    @Test
    public void getUserByIdWhenNoMatch() {
        Mockito.when(userManager.getUserById(1L)).thenReturn(new ResponseEntity<>(HttpStatus.NOT_FOUND));
        ResponseEntity<User> user = userController.getUserById(1L);
        assertThat(user.getStatusCode(), is(HttpStatus.NOT_FOUND));
    }
}
