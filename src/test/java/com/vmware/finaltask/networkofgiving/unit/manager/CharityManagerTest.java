package com.vmware.finaltask.networkofgiving.unit.manager;

import com.vmware.finaltask.networkofgiving.manager.CharityManager;
import com.vmware.finaltask.networkofgiving.model.Charity;
import com.vmware.finaltask.networkofgiving.repository.CharityRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.Objects;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@RunWith(SpringRunner.class)
@ExtendWith(MockitoExtension.class)
@SpringBootTest
public class CharityManagerTest {
    private static Charity charity1;
    private static Charity charity2;
    private static Charity charity3;

    @Mock
    private CharityRepository charityRepository;

    @InjectMocks
    private CharityManager charityManager;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Before
    public void init() {
        charity1 = new Charity.CharityBuilder("charity1","thumbnail1","description1",
                Charity.CharityType.MONEY_CHARITY).withMoney(100.0).build();
        charity2 = new Charity.CharityBuilder("charity2","thumbnail2","description2",
                Charity.CharityType.VOLUNTEER_CHARITY).withVolunteers(200).build();
        charity3 = new Charity.CharityBuilder("charity3","thumbnail3","description3",
                Charity.CharityType.MONEY_CHARITY).withMoney(300.0).build();
    }

    @Test
    public void getAllCharitiesWhenNoRecord() {
        Mockito.when(charityRepository.findAll()).thenReturn(Collections.emptyList());
        assertThat(charityManager.getAllCharities().getStatusCode(), is(HttpStatus.NO_CONTENT));
        Mockito.verify(charityRepository, Mockito.times(1)).findAll();
    }

    @Test
    public void getAllCharitiesWhenRecord() {
        Mockito.when(charityRepository.findAll()).thenReturn(Arrays.asList(charity1, charity2));
        assertThat(Objects.requireNonNull(charityManager.getAllCharities().getBody()).size(), is(2));
        assertThat(charityManager.getAllCharities().getBody().get(0), is(charity1));
        assertThat(charityManager.getAllCharities().getBody().get(1), is(charity2));
        Mockito.verify(charityRepository, Mockito.times(3)).findAll();
    }

    @Test
    public void getCharityById() {
        Mockito.when(charityRepository.findById(1L)).thenReturn(Optional.of(charity3));
        assertThat(charityManager.getCharityById(1L).getBody(), is(charity3));
        Mockito.verify(charityRepository, Mockito.times(1)).findById(1L);
    }
}
