package com.vmware.finaltask.networkofgiving.unit.manager;

import com.vmware.finaltask.networkofgiving.manager.UserManager;
import com.vmware.finaltask.networkofgiving.model.User;
import com.vmware.finaltask.networkofgiving.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.Objects;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@RunWith(SpringRunner.class)
@ExtendWith(MockitoExtension.class)
@SpringBootTest
public class UserManagerTest {
    private static User user1;
    private static User user2;
    private static User user3;

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserManager userManager;

    @Before
    public void setUp () {
        MockitoAnnotations.initMocks(this);;
    }

    @Before
    public void init() {
        user1 = new User("user1", "userName1", "pass1", "city1", User.Gender.MALE, 1);
        user2 = new User("user2", "userName2", "pass2", "city2", User.Gender.FEMALE, 2);
        user3 = new User("user3", "userName3", "pass3", "city3", User.Gender.OTHER, 3);
    }

    @Test
    public void getAllUsersWhenNoRecord() {
        Mockito.when(userRepository.findAll()).thenReturn(Collections.emptyList());
        assertThat(userManager.getAllUsers().getStatusCode(), is(HttpStatus.NO_CONTENT));
        Mockito.verify(userRepository, Mockito.times(1)).findAll();
    }

    @Test
    public void getAllUsersWhenRecord() {
        Mockito.when(userRepository.findAll()).thenReturn(Arrays.asList(user1, user2));
        assertThat(Objects.requireNonNull(userManager.getAllUsers().getBody()).size(), is(2));
        assertThat(userManager.getAllUsers().getBody().get(0), is(user1));
        assertThat(userManager.getAllUsers().getBody().get(1), is(user2));
        Mockito.verify(userRepository, Mockito.times(3)).findAll();

    }

    @Test
    public void getUserById() {
        Mockito.when(userRepository.findById(1L)).thenReturn(Optional.of(user1));
        assertThat(userManager.getUserById(1L).getBody(), is(user1));
        Mockito.verify(userRepository, Mockito.times(1)).findById(1L);
    }
}